extends Node3D

@export var SpeakerDeamp: float
var HighAnalysis: Vector2 = Vector2(0, 0)

var NextSceneChangeTimer = 0
var RainbowActivated = false
var TVActivated = false

signal ActivateCharacter(Name: String)

func _process(delta):
	NextSceneChangeTimer += delta
	
	# If the animation is already done, but there is need for more rainbow, give rainbow!
	if RainbowActivated == true and get_node("AnimationPlayer").is_playing() == false:
		get_node("AnimationPlayer").play("Rainbow")
	
	# Smooths motion of the speaker decreasing
	var Analysis = AudioServer.get_bus_effect_instance(1, 1).get_magnitude_for_frequency_range(0, 44100)
	if Analysis >= HighAnalysis:
		HighAnalysis = Analysis
	else:
		HighAnalysis /= 1.25
	
	# Microphone to object stuff
	for a in range(4):
		get_node("Speaker/Boombox").scale = Vector3(0.25 + (HighAnalysis[0] / SpeakerDeamp), 0.25 + (HighAnalysis[0] / SpeakerDeamp), 0.25 + (HighAnalysis[0] / SpeakerDeamp))
		# GPU stuff
		get_node("Speaker/Boombox/GPUParticles3D0").amount_ratio = HighAnalysis[0] + 0.01
		get_node("Speaker/Boombox/GPUParticles3D1").amount_ratio = HighAnalysis[0] + 0.01

func _on_microphone_finished():
	get_node("Background/Microphone").play()

func _input(event: InputEvent) -> void:
	# Switch characters on key press.
	if event.is_action_pressed("CharacterActivate"):
		ActivateCharacter.emit("Speaker")
		ActivateCharacter.emit("Camera")
	# Activate the rainbow when you press the key.
	if event.is_action_pressed("Rainbow"):
		RainbowActivated = !RainbowActivated
	# Do the TV stuff when you press the key.
	if event.is_action_pressed("ShowTV") and NextSceneChangeTimer >= 1.5:
		if TVActivated == false:
			get_node("AnimationPlayer").play("DropTV")
			get_node("DropTV/Drop").play()
		else:
			get_node("AnimationPlayer").play("LiftTV")
			get_node("DropTV/Lift").play()
		TVActivated = !TVActivated
		# reset the scene timer.
		NextSceneChangeTimer = 0
