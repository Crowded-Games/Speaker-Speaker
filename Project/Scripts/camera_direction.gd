extends Node

var CameraDirectionRadians: Basis
var Direction

@onready var VectorXZ = Input.get_vector("Left", "Right", "Forward", "Backward")

func _process(_delta: float) -> void:
	VectorXZ = Input.get_vector("Left", "Right", "Forward", "Backward")
	var VectorY = Input.get_axis("Camera Down", "Camera Up")
	
	Direction = CameraDirectionRadians * Vector3(VectorXZ.x, VectorY, VectorXZ.y).normalized()
