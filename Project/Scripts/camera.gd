extends CharacterBody3D

@export var MouseSensitivity: float = 1
var CharacterActive = true

func _ready():
	Input.mouse_mode = Input.MOUSE_MODE_CAPTURED

func _process(_delta: float):
	var Speed = 5

	# Movement
	if CharacterActive == true:
		velocity = CameraDirection.Direction * Speed
	else:
		velocity = velocity * 0
	
	# Send the basis
	CameraDirection.CameraDirectionRadians = transform.basis
	
	move_and_slide()

# move around the camera
func _input(event: InputEvent):
	if event is InputEventMouseMotion and CharacterActive == true:
		self.rotation_degrees.y -= event.relative[0] * MouseSensitivity
		get_node("Camera3D").rotation_degrees.x = clampf(get_node("Camera3D").rotation_degrees.x + -event.relative[1] * MouseSensitivity, -90, 90)

func _on_main_scene_activate_character(Name: String):
	if Name == "Camera" and CharacterActive == false:
		CharacterActive = true
	elif Name == "Camera" and CharacterActive == true:
		CharacterActive = false
