extends CharacterBody3D

@export var MouseSensitivity: float = 1
var CharacterActive = false

func _process(delta: float) -> void:
	var Speed = 1.75
	
	# Movement
	if CharacterActive == true:
		velocity.x = CameraDirection.Direction.x * Speed
		velocity.z = CameraDirection.Direction.z * Speed
		# I give up. Until I make the SFM portion, this is what we get.
		get_node("AnimationTree").set("parameters/blend_position", Vector2(CameraDirection.VectorXZ.x, CameraDirection.VectorXZ.y))
	else:
		velocity.x = velocity.x * 0
		velocity.z = velocity.z * 0
		get_node("AnimationTree").set("parameters/blend_position", Vector2(0, 0))
	
	# Set gravity and speeds
	if !is_on_floor():
		velocity.y += -ProjectSettings.get("physics/3d/default_gravity") * delta
	else:
		velocity.y = 0
	
	move_and_slide()

func _on_main_scene_activate_character(Name: String) -> void:
	if Name == "Speaker" and CharacterActive == false:
		CharacterActive = true
	elif Name == "Speaker" and CharacterActive == true:
		CharacterActive = false

func _input(event: InputEvent):
	if CharacterActive == true:
		if event is InputEventMouseMotion:
			rotation_degrees.y -= event.relative[0] * MouseSensitivity
